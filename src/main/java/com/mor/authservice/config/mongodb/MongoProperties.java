package com.mor.authservice.config.mongodb;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "spring.data.mongodb")
@Getter
@Setter
public class MongoProperties {

  private String host;

  private int port;

  private String username;

  private String password;

  private String database;

}
