package com.mor.authservice.config.mongodb.changelogs;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mor.authservice.domain.AuthClientDetails;
import com.mor.authservice.domain.User;
import com.mor.authservice.enums.Authorities;
import java.util.HashSet;
import java.util.Set;
import org.springframework.data.mongodb.core.MongoTemplate;

@ChangeLog
public class InitialValuesChangeLog {

  /**
   * seed new client with. {@value #clientID} browser {@value #clientSecret} 1234 {@value #scope}
   * ui
   *
   * @param mongoTemplate - mongoDB template runner
   */
  @ChangeSet(order = "001", id = "insertBrowserClientDetails", author = "khanhdq")
  public void insertBrowserClientDetails(MongoTemplate mongoTemplate) {
    AuthClientDetails browserClientDetails = new AuthClientDetails();
    browserClientDetails.setClientId("browser");
    browserClientDetails.setClientSecret(
        "$2a$04$/jfYgSyLvzqAhpPKqmpbhe71Mc1Xz4DRPW5JC8CnaQN/I3HjTkmmi");
    browserClientDetails.setScopes("ui");
    browserClientDetails.setGrantTypes("refresh_token,password");

    mongoTemplate.save(browserClientDetails);
  }

  /**
   * seed new client with. {@value #grantType} password {@value #userName} admin {@value #password}
   * 1234
   *
   * @param mongoTemplate - mongoDB template runner
   */
  @ChangeSet(order = "002", id = "insertAdminToTestAuthentication", author = "khanhdq")
  public void insertAdminToTestAuthentication(MongoTemplate mongoTemplate) {
    Set<Authorities> authorities = new HashSet<>();
    authorities.add(Authorities.ROLE_ADMIN);

    User user = new User();
    user.setActivated(true);
    user.setAuthorities(authorities);
    user.setPassword("$2a$04$x.HM7fa80Cwnq386jTqG..Av6p2PHU59J/zH/6cT99YzSj6M9Ty3u");
    user.setUsername("admin");
    user.setSaleId("");

    mongoTemplate.save(user);
  }

  /**
   * seed new client with. {@value #grantType} password {@value #userName} user {@value #password}
   * 1234
   *
   * @param mongoTemplate - mongoDB template runner
   */
  @ChangeSet(order = "003", id = "insertUserToTestAuthentication", author = "khanhdq")
  public void insertUserToTestAuthentication(MongoTemplate mongoTemplate) {
    Set<Authorities> authorities = new HashSet<>();
    authorities.add(Authorities.ROLE_HOST);

    User user = new User();
    user.setActivated(true);
    user.setAuthorities(authorities);
    user.setEmail("khanhdq@mor.com.vn");
    user.setPhone("0968442766");
    user.setIdentifyNo("11111111111");
    user.setPassword("$2a$04$MsSBRkoqXjw1lmqm8qmbnOwwqJS/.ryy/BZ/1iD8jMxrcAIpWClva");
    user.setUsername("user");

    mongoTemplate.save(user);
  }

  /**
   * seed new client with. {@value #clientID} user-service {@value #clientSecret} 1234
   *
   * @param mongoTemplate - mongoDB template runner
   */
  @ChangeSet(order = "004", id = "insertUserServiceClientDetails", author = "khanhdq")
  public void insertUserServiceClientDetails(MongoTemplate mongoTemplate) {
    AuthClientDetails userServiceClientDetails = new AuthClientDetails();
    userServiceClientDetails.setClientId("user-service");
    userServiceClientDetails.setClientSecret(
        "$2a$04$/jfYgSyLvzqAhpPKqmpbhe71Mc1Xz4DRPW5JC8CnaQN/I3HjTkmmi");
    userServiceClientDetails.setScopes("server");
    userServiceClientDetails.setGrantTypes("refresh_token,client_credentials");

    mongoTemplate.save(userServiceClientDetails);
  }

  /**
   * seed new client with. {@value #clientID} user-service {@value #clientSecret} 1234
   *
   * @param mongoTemplate - mongoDB template runner
   */
  @ChangeSet(order = "005", id = "insertAccountServiceClientDetails", author = "khanhdq")
  public void insertAccountServiceClientDetails(MongoTemplate mongoTemplate) {
    AuthClientDetails userServiceClientDetails = new AuthClientDetails();
    userServiceClientDetails.setClientId("account-service");
    userServiceClientDetails.setClientSecret(
        "$2a$04$/jfYgSyLvzqAhpPKqmpbhe71Mc1Xz4DRPW5JC8CnaQN/I3HjTkmmi");
    userServiceClientDetails.setScopes("server");
    userServiceClientDetails.setGrantTypes("refresh_token,client_credentials");

    mongoTemplate.save(userServiceClientDetails);
  }

  /**
   * seed new client with. {@value #clientID} user-service {@value #clientSecret} 1234
   *
   * @param mongoTemplate - mongoDB template runner
   */
  @ChangeSet(order = "006", id = "insertTestcaseServiceClientDetails", author = "khanhdq")
  public void insertManagementServiceClientDetails(MongoTemplate mongoTemplate) {
    AuthClientDetails userServiceClientDetails = new AuthClientDetails();
    userServiceClientDetails.setClientId("management-service");
    userServiceClientDetails.setClientSecret(
        "$2a$04$/jfYgSyLvzqAhpPKqmpbhe71Mc1Xz4DRPW5JC8CnaQN/I3HjTkmmi");
    userServiceClientDetails.setScopes("server");
    userServiceClientDetails.setGrantTypes("refresh_token,client_credentials");

    mongoTemplate.save(userServiceClientDetails);
  }

  /**
   * seed new client with. {@value #clientID} user-service {@value #clientSecret} 1234
   *
   * @param mongoTemplate - mongoDB template runner
   */
  @ChangeSet(order = "007", id = "insertReportServiceClientDetails", author = "khanhdq")
  public void insertHouseRentalServiceClientDetails(MongoTemplate mongoTemplate) {
    AuthClientDetails userServiceClientDetails = new AuthClientDetails();
    userServiceClientDetails.setClientId("house-rental-service");
    userServiceClientDetails.setClientSecret(
        "$2a$04$/jfYgSyLvzqAhpPKqmpbhe71Mc1Xz4DRPW5JC8CnaQN/I3HjTkmmi");
    userServiceClientDetails.setScopes("server");
    userServiceClientDetails.setGrantTypes("refresh_token,client_credentials");

    mongoTemplate.save(userServiceClientDetails);
  }

  /**
   * seed new client with. {@value #clientID} user-service {@value #clientSecret} 1234
   *
   * @param mongoTemplate - mongoDB template runner
   */
  @ChangeSet(order = "008", id = "insertNotificationServiceClientDetails", author = "khanhdq")
  public void insertNotificationServiceClientDetails(MongoTemplate mongoTemplate) {
    AuthClientDetails userServiceClientDetails = new AuthClientDetails();
    userServiceClientDetails.setClientId("notification-service");
    userServiceClientDetails.setClientSecret(
        "$2a$04$/jfYgSyLvzqAhpPKqmpbhe71Mc1Xz4DRPW5JC8CnaQN/I3HjTkmmi");
    userServiceClientDetails.setScopes("server");
    userServiceClientDetails.setGrantTypes("refresh_token,client_credentials");

    mongoTemplate.save(userServiceClientDetails);
  }

  @ChangeSet(order = "009", id = "insertInfoServiceClientDetails", author = "khanhdq")
  public void insertInfoServiceClientDetails(MongoTemplate mongoTemplate) {
    AuthClientDetails userServiceClientDetails = new AuthClientDetails();
    userServiceClientDetails.setClientId("info-service");
    userServiceClientDetails.setClientSecret(
        "$2a$04$/jfYgSyLvzqAhpPKqmpbhe71Mc1Xz4DRPW5JC8CnaQN/I3HjTkmmi");
    userServiceClientDetails.setScopes("server");
    userServiceClientDetails.setGrantTypes("refresh_token,client_credentials");

    mongoTemplate.save(userServiceClientDetails);
  }
}
