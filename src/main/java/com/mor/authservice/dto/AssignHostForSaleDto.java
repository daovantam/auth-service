package com.mor.authservice.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AssignHostForSaleDto {

  private String hostId;
  private String saleId;
}
