package com.mor.authservice.dto;

import com.mor.authservice.enums.Authorities;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ChangeRoleDto {

  private String id;

  private Authorities authorities;
}
