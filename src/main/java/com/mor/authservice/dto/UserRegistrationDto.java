package com.mor.authservice.dto;

import java.io.Serializable;
import lombok.ToString;

@ToString
public class UserRegistrationDto implements Serializable {

  private static final long serialVersionUID = 1L;

  private String username;

  private String password;

  private String identityNo;

  private String phone;

  private String email;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getIdentityNo() {
    return identityNo;
  }

  public void setIdentityNo(String identityNo) {
    this.identityNo = identityNo;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
