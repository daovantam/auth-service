package com.mor.authservice.service.impl;

import com.mor.authservice.domain.User;
import com.mor.authservice.dto.ChangePasswordDto;
import com.mor.authservice.dto.ResetPasswordDto;
import com.mor.authservice.dto.UserRegistrationDto;
import com.mor.authservice.enums.Authorities;
import com.mor.authservice.repository.UserRepository;
import com.mor.authservice.service.UserService;
import com.mor.authservice.util.Converter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Random;
import javassist.NotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

  private final PasswordEncoder passwordEncoder;
  private final UserRepository userRepository;
  private final Converter converter;

  public UserServiceImpl(PasswordEncoder passwordEncoder, UserRepository userRepository,
      Converter converter) {
    this.passwordEncoder = passwordEncoder;
    this.userRepository = userRepository;
    this.converter = converter;
  }

  @Override
  public User create(UserRegistrationDto userRegistrationDto) {
    User user = converter.toUser(userRegistrationDto);
    throwIfUsernameExists(user.getUsername());

    String hash = passwordEncoder.encode(user.getPassword());
    user.setPassword(hash);
    user.setActivated(true); // TODO send sms or email with code for activation
    user.setAuthorities(new HashSet<>(Collections.singletonList(Authorities.ROLE_HOST)));

    // TODO other routines on account creation

    return userRepository.save(user);
  }

  @Override
  public ResetPasswordDto resetPassword(String username) throws NotFoundException {
    int min = 100000;
    int max = 999999;
    if ("admin".equals(username)) {
      throw new RuntimeException("admin cannot reset password. Please connect to HouseRental Team");
    }
    int newPassword = new Random().nextInt((max - min) + 1);
    User user = userRepository.findByUsername(username)
        .orElseThrow(() -> new NotFoundException(username + " Not found"));
    String hash = passwordEncoder.encode(String.valueOf(newPassword));
    user.setPassword(hash);
    userRepository.save(user);
    ResetPasswordDto resetPasswordDto = new ResetPasswordDto();
    resetPasswordDto.setEmail(user.getEmail());
    resetPasswordDto.setPassword(String.valueOf(newPassword));
    return resetPasswordDto;
  }

  @Override
  public User changePassword(ChangePasswordDto changePasswordDto) throws NotFoundException {
    User user = userRepository.findByUsername(changePasswordDto.getUsername())
        .orElseThrow(() -> new NotFoundException(
            changePasswordDto.getUsername() + " not found"));
    boolean passwordMatch = BCrypt.checkpw(changePasswordDto.getOldPassword(), user.getPassword());
    if (passwordMatch) {
      user.setPassword(passwordEncoder.encode(changePasswordDto.getNewPassword()));
    }
    return userRepository.save(user);
  }


  private void throwIfUsernameExists(String username) {
    Optional<User> existingUser = userRepository.findByUsername(username);
    existingUser.ifPresent((user) -> {
      throw new IllegalArgumentException("User not available");
    });
  }
}
