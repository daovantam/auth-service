package com.mor.authservice.service.impl;

import com.mor.authservice.domain.User;
import com.mor.authservice.dto.AssignHostForSaleDto;
import com.mor.authservice.dto.ChangeRoleDto;
import com.mor.authservice.dto.UserDto;
import com.mor.authservice.enums.Authorities;
import com.mor.authservice.repository.UserRepository;
import com.mor.authservice.service.ManagementService;
import com.mor.authservice.util.Converter;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManagementServiceImpl implements ManagementService {

  private final UserRepository userRepository;
  private final Converter converter;

  @Autowired
  public ManagementServiceImpl(UserRepository userRepository,
      Converter converter) {
    this.userRepository = userRepository;
    this.converter = converter;
  }

  @Override
  public List<UserDto> getUsersNotAssignSale() {
    Set<Authorities> authorities = new HashSet<>(
        Collections.singleton(Authorities.ROLE_HOST));
    List<User> hosts = userRepository.findAllBySaleIdIsNullAndAuthorities(authorities);
    return hosts.stream().map(converter::toDto).collect(Collectors.toList());
  }

  @Override
  public List<UserDto> getSales() {
    Set<Authorities> authorities = new HashSet<>(
        Collections.singleton(Authorities.ROLE_RM));
    List<User> sales = userRepository.findAllByAuthorities(authorities);
    return sales.stream().map(converter::toDto).collect(Collectors.toList());
  }

  @Override
  public UserDto assignHostForSale(AssignHostForSaleDto dto) throws Exception {
    User host = findUserByIdAndRole(dto.getHostId(), Authorities.ROLE_HOST);
    if (host == null) {
      throw new NotFoundException("hostId not found");
    }
    if (dto.getSaleId().isEmpty()) {
      host.setSaleId(null);
    } else {
      User sale = findUserByIdAndRole(dto.getSaleId(), Authorities.ROLE_RM);
      if (sale == null) {
        throw new NotFoundException("saleId not found");
      }
      host.setSaleId(sale.getId());
    }

    return converter.toDto(userRepository.save(host));
  }

  @Override
  public UserDto changeRoleForUser(ChangeRoleDto changeRoleDto) throws NotFoundException {
    Optional<User> userOptional = userRepository.findById(changeRoleDto.getId());
    Set<Authorities> authorities = new HashSet<>(
        Collections.singletonList(changeRoleDto.getAuthorities()));
    if (userOptional.isPresent()) {
      User update = userOptional.get();
      update.setAuthorities(authorities);
      return converter.toDto(userRepository.save(update));
    }
    throw new NotFoundException("User not found");
  }

  @Override
  public List<UserDto> getUserBySaleId(String saleName) {
    Optional<User> userOptional = userRepository.findByUsername(saleName);
    List<User> users = userRepository.findAllBySaleId(userOptional.get().getId());
    return users.stream().map(converter::toDto).collect(Collectors.toList());
  }

  private User findUserByIdAndRole(String id, Authorities authorities) {
    Set<Authorities> authoritiesSet = new HashSet<>(
        Collections.singleton(authorities));
    return userRepository.findByIdAndAuthorities(id, authoritiesSet);
  }
}
