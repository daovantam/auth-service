package com.mor.authservice.service;

import com.mor.authservice.dto.AssignHostForSaleDto;
import com.mor.authservice.dto.ChangeRoleDto;
import com.mor.authservice.dto.UserDto;
import java.util.List;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

@Service
public interface ManagementService {

  List<UserDto> getUsersNotAssignSale();

  List<UserDto> getSales();

  UserDto assignHostForSale(AssignHostForSaleDto dto) throws Exception;

  UserDto changeRoleForUser(ChangeRoleDto changeRoleDto) throws NotFoundException;

  List<UserDto> getUserBySaleId(String saleName);
}
