package com.mor.authservice.service;

import com.mor.authservice.domain.User;
import com.mor.authservice.dto.ChangePasswordDto;
import com.mor.authservice.dto.ResetPasswordDto;
import com.mor.authservice.dto.UserRegistrationDto;
import javassist.NotFoundException;

public interface UserService {

  User create(UserRegistrationDto user);

  ResetPasswordDto resetPassword(String username) throws NotFoundException;

  User changePassword(ChangePasswordDto changePasswordDto) throws NotFoundException;
}
