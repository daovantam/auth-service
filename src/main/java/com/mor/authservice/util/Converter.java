package com.mor.authservice.util;

import com.mor.authservice.constant.RegexConstant;
import com.mor.authservice.domain.User;
import com.mor.authservice.dto.UserDto;
import com.mor.authservice.dto.UserRegistrationDto;
import org.springframework.stereotype.Component;

@Component
public class Converter {

  public UserDto toDto(User user) {
    UserDto userDto = new UserDto();
    userDto.setId(user.getId());
    userDto.setUsername(user.getUsername());
    return userDto;
  }

  public User toUser(UserRegistrationDto userRegistration) {
    User user = new User();
    user.setUsername(userRegistration.getUsername());
    if (userRegistration.getIdentityNo().matches(RegexConstant.IDENTITY_NO) || userRegistration
        .getIdentityNo().matches(RegexConstant.IDENTITY_NO_12)) {
      user.setIdentifyNo(userRegistration.getIdentityNo());
    }
    if (!userRegistration.getEmail().isEmpty()) {
      user.setEmail(userRegistration.getEmail());
    } else {
      user.setEmail(null);
    }
    user.setPhone(userRegistration.getPhone());
    user.setPassword(userRegistration.getPassword());
    return user;
  }
}
