package com.mor.authservice.constant;

public class RegexConstant {

  public static final String EMAIL = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
  public static final String IDENTITY_NO = "^([0-9]{9})$";
  public static final String IDENTITY_NO_12 = "^([0-9]{12})$";
}
