package com.mor.authservice.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResponseStatusCodeEnum {
  SUCCESS("PTP200", "Success"),
  CREATED("PTP201", "Created"),
  NO_PERMISSION("PTP40000", "The client has no permission calling this API"),
  INVALID_PARAMETER("PTP40001", "Invalid request parameter"),
  INTERNAL_SERVER_ERROR("PTP50000", "Internal server error"),
  SCOPE_NOT_FOUND("PTP40004", "Scope not found"),
  RESOURCE_NOT_FOUND("PTP4040", "Resource not found"),
  INCORRECT_FILE_FORMAT("PTP400", "Incorrect file format"),
  CODE_INVALID("PTP40002", "Project code has already exists, please select another code");
  private String code;
  private String message;
}
