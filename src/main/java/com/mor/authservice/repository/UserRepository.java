package com.mor.authservice.repository;

import com.mor.authservice.domain.User;
import com.mor.authservice.enums.Authorities;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

  Optional<User> findByUsername(String username);

  List<User> findAllByAuthorities(Set<Authorities> authorities);

  List<User> findAllByIdIn(List<String> ids);

  List<User> findAllBySaleIdIsNullAndAuthorities(Set<Authorities> authorities);

  User findByIdAndAuthorities(String id, Set<Authorities> authorities);

  List<User> findAllBySaleId(String id);

}
