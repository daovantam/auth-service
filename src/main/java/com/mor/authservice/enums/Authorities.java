package com.mor.authservice.enums;

import org.springframework.security.core.GrantedAuthority;

public enum Authorities implements GrantedAuthority {

  ROLE_ADMIN, ROLE_RM, ROLE_HOST;

  @Override
  public String getAuthority() {
    return name();
  }
}
