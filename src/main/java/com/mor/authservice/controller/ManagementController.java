package com.mor.authservice.controller;

import com.mor.authservice.dto.AssignHostForSaleDto;
import com.mor.authservice.dto.ChangeRoleDto;
import com.mor.authservice.dto.UserDto;
import com.mor.authservice.factory.ResponseFactory;
import com.mor.authservice.service.ManagementService;
import java.util.List;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/manage")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class ManagementController {

  private final ManagementService managementService;
  private final ResponseFactory responseFactory;

  @Autowired
  public ManagementController(ManagementService managementService,
      ResponseFactory responseFactory) {
    this.managementService = managementService;
    this.responseFactory = responseFactory;
  }

  @GetMapping("host-un-assign")
  public ResponseEntity<?> getHostUnAssign() {
    List<UserDto> userDtos = managementService.getUsersNotAssignSale();
    return responseFactory.success(userDtos, List.class);
  }

  @GetMapping("sale")
  public ResponseEntity<?> getSales() {
    List<UserDto> sales = managementService.getSales();
    return responseFactory.success(sales, List.class);
  }

  @PutMapping("change-role")
  public ResponseEntity<?> changeRole(@RequestBody ChangeRoleDto changeRoleDto)
      throws NotFoundException {
    UserDto userDto = managementService.changeRoleForUser(changeRoleDto);
    return responseFactory.success(userDto, UserDto.class);
  }

  @PutMapping("assign")
  public ResponseEntity<?> assignHost(@RequestBody AssignHostForSaleDto dto)
      throws Exception {
    UserDto userDto = managementService.assignHostForSale(dto);
    return responseFactory.success(userDto, UserDto.class);
  }
}
