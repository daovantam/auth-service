package com.mor.authservice.controller;

import com.mor.authservice.dto.UserDto;
import com.mor.authservice.factory.ResponseFactory;
import com.mor.authservice.service.ManagementService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/sale")
public class SaleController {

  private final ManagementService managementService;

  private final ResponseFactory responseFactory;

  @Autowired
  public SaleController(ManagementService managementService,
      ResponseFactory responseFactory) {
    this.managementService = managementService;
    this.responseFactory = responseFactory;
  }

  @GetMapping("/{name}")
  public ResponseEntity<?> getAllHostOfSale(@PathVariable("name") String saleName) {
    List<UserDto> hostOfSale = managementService.getUserBySaleId(saleName);
    return responseFactory.success(hostOfSale, List.class);
  }
}
