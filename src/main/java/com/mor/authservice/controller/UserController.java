package com.mor.authservice.controller;

import com.mor.authservice.domain.User;
import com.mor.authservice.dto.ChangePasswordDto;
import com.mor.authservice.dto.ResetPasswordDto;
import com.mor.authservice.dto.UserDto;
import com.mor.authservice.dto.UserRegistrationDto;
import com.mor.authservice.factory.ResponseFactory;
import com.mor.authservice.service.UserService;
import com.mor.authservice.util.Converter;
import java.security.Principal;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
  private final UserService userService;
  private final Converter converter;

  private final ResponseFactory responseFactory;

  @Autowired
  public UserController(UserService userService, Converter converter,
      ResponseFactory responseFactory) {
    this.userService = userService;
    this.converter = converter;
    this.responseFactory = responseFactory;
  }

  @GetMapping("/current")
  public Principal getUser(Principal principal) {
    return principal;
  }

  @PostMapping
  @PreAuthorize("#oauth2.hasScope('server')")
  public ResponseEntity<?> createUser(@RequestBody UserRegistrationDto userRegistration) {
    LOGGER.info("START create new user with data = {}", userRegistration);

    User savedUser = userService.create(userRegistration);

    LOGGER.info("END create new user");
    return responseFactory.success(converter.toDto(savedUser), UserDto.class);
  }

  @GetMapping("/reset-password/{username}")
  @PreAuthorize("#oauth2.hasScope('server')")
  public ResponseEntity<?> resetPassword(@PathVariable("username") String username)
      throws NotFoundException {
    ResetPasswordDto reset = userService.resetPassword(username);
    return responseFactory.success(reset, ResetPasswordDto.class);
  }

  @PostMapping("/change-password")
  public ResponseEntity<?> changePassword(@RequestBody ChangePasswordDto changePasswordDto)
      throws NotFoundException {
    User user = userService.changePassword(changePasswordDto);

    return responseFactory.success(converter.toDto(user), UserDto.class);
  }
}
